﻿#include <iostream>
using namespace std;

class Animal
{
public:
    virtual void Voice() 
    {
        cout << "selected animal`s voice";
    }
};

class Dog : public Animal
{
public:
    Dog() {}

    void Voice() override
    {
        cout << "Woof!\n";
    }
};

class Cat : public Animal
{
public:
    Cat() {}

    void Voice() override
    {
        cout << "Meow!\n";
    }
};

class Sparrow : public Animal
{
public:
    Sparrow() {}

    void Voice() override
    {
        cout << "Cheep - cheep!\n";
    }
};

int main()
{
    Animal* D = new Dog();
    Animal* C = new Cat();
    Animal* S = new Sparrow();

    Animal* AnimalsArray[3] = { D, C, S };
    for (int i = 0; i < 3; i++)
    {
        AnimalsArray[i]->Voice();
    }




}

/*
В функции main создать массив указателей типа Animal и заполнить этот массив объектами созданных классов.
Затем пройтись циклом по массиву, вызывая на каждом элементе массива метод Voice().
Протестировать его работу, должны выводиться сообщения из ваших классов наследников Animal.*/